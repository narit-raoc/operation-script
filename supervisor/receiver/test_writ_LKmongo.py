import pymongo
from pymongo import MongoClient
import datetime

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')

db = client.LK_monitor # use database myDB
coll = db.Monitor

temperatureStats = {
        "sensorName": "Lband",
        "T15": 15,
        "T70": 41,
        "date": datetime.datetime.now()
    }


if(temperatureStats['sensorName'] == "Lband"):
  coll.insert_one(temperatureStats)


temperatureStats = {
        "sensorName": "Kband",
        "T15": 16,
        "T70": 50,
        "date": datetime.datetime.now()
    }

if(temperatureStats['sensorName'] == "Kband"):
  coll.insert_one(temperatureStats)


last_N = 10
# query last_N data from collection
data_row = coll.find().skip(coll.count() - last_N)
for x in data_row:
  print x