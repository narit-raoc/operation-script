import pymongo
from pymongo import MongoClient
import datetime

# Server Parameters
client = pymongo.MongoClient('mongodb://root:rootpassword@localhost:27017/')
db = client.visalar_monitor # use database myDB
coll = db.Monitor


temperatureStatsCollection = db["Monitor"]
temperatureStats = {
        "sensorName": "Visalar",
        "Temp": 20,
        "Humid": 45,
        "date": datetime.datetime.now()
    }

if(temperatureStats['sensorName'] == "Visalar"):
  coll.insert_one(temperatureStats)

x = coll.find_one()

print(x)

for x in coll.find():
  print(x)
