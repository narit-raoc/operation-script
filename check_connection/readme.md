# script check connection
Check connection to servers by ping from computer at Narit

We use python Supervisor to run the script as a service 

servers include
- 8 nodes servers (90.1-8)
- 10 nodes servers (90.11-20)
## setup environment variables
copy .env.example and create new file name .env and fill out all credentials needed

In repository directory,
```
cp .env.example .env
```
## To setup supervisor service  
[reference](https://jayden-chua.medium.com/use-supervisor-to-run-your-python-tests-13e91171d6d3)

``` 
sudo apt-get install supervisor
sudo supervisord
```
Create a configuration file for this process and name it
```
sudo nano /etc/supervisor/conf.d/check_connection.conf
```
paste this in a config file (replace **${path to repo}** with your correct path) 
```
[program:check_connection_service]
command=python -u script.py
directory=/${path to repo}/maintenance-script/check_connection
stdout_logfile=/${path to repo}/maintenance-script/check_connection/output.log
redirect_stderr=true
```

Note: You might need to change path to python if using pyenv. To get path to python, in terminal, run `which python` copy the result and replace to line `command=[replace here] -u script.py`

Using supervisor

```
sudo supervisorctl
```

```
supervisor > reread
supervisor > add check_connection_service
supervisor > status
```