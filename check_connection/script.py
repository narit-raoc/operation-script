#!/usr/bin/python3
# coding=utf-8

import time
import subprocess
import requests
from dotenv import dotenv_values

config = dotenv_values(".env")

SERVERS = [
    {
        'ip': '192.168.90.1',
        'status': 'active',
        'name': 'usb-01'
    },
    {
        'ip': '192.168.90.2',
        'status': 'active',
        'name': 'usb-02'
    },
    {
        'ip': '192.168.90.3',
        'status': 'active',
        'name': 'usb-03'
    },
    {
        'ip': '192.168.90.4',
        'status': 'active',
        'name': 'usb-04'
    },
    {
        'ip': '192.168.90.5',
        'status': 'active',
        'name': 'usb-05'
    },
    {
        'ip': '192.168.90.6',
        'status': 'active',
        'name': 'usb-06'
    },
    {
        'ip': '192.168.90.7',
        'status': 'active',
        'name': 'usb-07'
    },
    {
        'ip': '192.168.90.8',
        'status': 'active',
        'name': 'usb-08'
    },
    {
        'ip': '192.168.90.11',
        'status': 'active',
        'name': 'pop-11'
    },
    {
        'ip': '192.168.90.12',
        'status': 'active',
        'name': 'pop-12'
    },
    {
        'ip': '192.168.90.13',
        'status': 'active',
        'name': 'pop-13'
    },
    {
        'ip': '192.168.90.14',
        'status': 'active',
        'name': 'pop-14'
    },
    {
        'ip': '192.168.90.15',
        'status': 'active',
        'name': 'pop-15'
    },
    {
        'ip': '192.168.90.16',
        'status': 'active',
        'name': 'pop-16'
    },
    {
        'ip': '192.168.90.17',
        'status': 'active',
        'name': 'pop-17'
    },
    {
        'ip': '192.168.90.18',
        'status': 'active',
        'name': 'pop-18'
    },
    {
        'ip': '192.168.90.19',
        'status': 'active',
        'name': 'pop-19'
    },
    {
        'ip': '192.168.90.20',
        'status': 'active',
        'name': 'pop-20'
    },
    {
        'ip': '192.168.90.51',
        'status': 'active',
        'status': 'storage'
    },
    {
        'ip': '192.168.90.101',
        'status': 'active',
        'name': 'packetizer'
    },
    {
        'ip': '192.168.93.93',
        'status': 'active',
        'name': 'e3'
    }
]


CHECK_INTERVAL = int(config['CHECK_INTERVAL']) * 60  # normal situation ping every 1 minute
MAINTENANCE_INTERVAL = int(config['MAINTENANCE_INTERVAL']) * 60  # time to skip checking after server is down 30 minutes


def send_message(msg, img=None, sticker_package=None, sticker_id=None):
    """Send a LINE Notify message (with or without an image)."""
    headers = {'Authorization': 'Bearer ' + config['LINE_TOKEN']}
    payload = {'message': msg}
    files = {'imageFile': open(img, 'rb')} if img else None
    if(sticker_package and sticker_id):
        payload['stickerPackageId'] = sticker_package
        payload['stickerId'] = sticker_id

    r = requests.post(
        config['LINE_URL'],
        headers=headers,
        params=payload,
        files=files
    )

    if files:
        files['imageFile'].close()
    return r.status_code


while True:
    hasDownServer = False
    for index, server in enumerate(SERVERS):
        if(
            server['status'] == 'active' or
            (server['status'] == 'down' and time.time() -
             server['downTime'] >= MAINTENANCE_INTERVAL)
        ):
            response = subprocess.call(['ping', '-c', '1', server['ip']])
            if(response == 1):  # cannot ping
                if(server['status'] == 'active'):
                    hasDownServer = True
                    SERVERS[index]['status'] = 'down'
                    send_message('server {} ({}) is down'.format(server['ip'], server['name']))
                SERVERS[index]['downTime'] = time.time()
            elif(response == 0 and server['status'] == 'down'):  # ping success
                send_message(
                    'server {} is back to normal operation'.format(
                        server['ip']),
                    sticker_package=446,
                    sticker_id=1989
                )
                SERVERS[index]['status'] = 'active'
    if(hasDownServer):
        send_message(
            'Please check all servers condition and fix asap',
            sticker_package=446,
            sticker_id=2005
        )

    time.sleep(CHECK_INTERVAL)
